# ~*~ encoding: utf-8 ~*~
from migrate.versioning import api

from app.config import SQLALCHEMY_DATABASE_URI
from app.config import SQLALCHEMY_MIGRATE_REPO


if __name__ == '__main__':
    v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
    api.downgrade(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, v - 1)

    print('Current database version: '
          + str(api.db_version(SQLALCHEMY_DATABASE_URI,
                               SQLALCHEMY_MIGRATE_REPO)))
