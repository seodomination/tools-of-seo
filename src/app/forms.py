# ~*~ encoding: utf-8 ~*~
from flask.ext.wtf import Form
from wtforms import BooleanField, StringField
from wtforms.validators import Required


class LoginForm(Form):

    username = StringField('username', validators=[Required()])
    password = StringField('password', validators=[Required()])
    remember_me = BooleanField('remember_me', default=False)
