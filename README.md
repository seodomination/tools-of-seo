# What are SEO tools?

There are some good free tools for [SEO consultant London](http://seodomination.co.uk/seo-expert-london/). There are so many fantastic and interesting tools on the market
that help you to manage search engines. Some of them are listed below:

[http://stackoverflow.com/users/7856334/seo-domination?tab=profile](http://stackoverflow.com/users/7856334/seo-domination?tab=profile)

# Tools of SEO

1) Search analytics tools:
    It help you to fulfill your dreams regarding to Marketing and SEO fields and improves your
    baseline for where you are now. 

2) Keyword Research Tools:
    It discovers the focus keywords which are popular for users.
    
3) On-Page Audits:
    It helps to compare your performance with the other leading competitors and finds your optimize pages.

4) Competitive Research Tools:
    It helps to tell you which keyword is the targeted keyword for the competitors.

5) Search engine Ranking Checker:
     It helps to determine your marketing power and check how to improve your search engine rankings.

[https://www.joomlart.com/user/seodomination/](https://www.joomlart.com/user/seodomination/)
[https://issuu.com/seodomination](https://issuu.com/seodomination)
[http://www.nature.com/protocolexchange/labgroups/647059](http://www.nature.com/protocolexchange/labgroups/647059)
[http://www.colourlovers.com/lover/seodomination](http://www.colourlovers.com/lover/seodomination)
[https://www.codeplex.com/site/users/view/seodomination](https://www.codeplex.com/site/users/view/seodomination)
[http://www.scoop.it/u/seodomination](http://www.scoop.it/u/seodomination)
[https://www.smashwords.com/profile/view/seodomination](https://www.smashwords.com/profile/view/seodomination)
[http://forum.support.xerox.com/t5/user/viewprofilepage/user-id/179692](http://forum.support.xerox.com/t5/user/viewprofilepage/user-id/179692)
[https://www.instapaper.com/p/5763791](https://www.instapaper.com/p/5763791)


The Sitemap

SEO for Craft boasts an extremely powerful, yet simple to use Sitemap manager. With automatic support for all your site’s sections and categories (with localisations taken into account), and the ability to easily add custom URLs (useful for public templates that aren’t content managed), keeping your sitemap up-to-date has never been easier.

[http://photo.accuweather.com/photogallery/user/33775/](http://photo.accuweather.com/photogallery/user/33775/)
[http://www.purevolume.com/listeners/seodomination](http://www.purevolume.com/listeners/seodomination)
[https://community.linksys.com/t5/user/viewprofilepage/user-id/797445](https://community.linksys.com/t5/user/viewprofilepage/user-id/797445)
[https://java.net/people/1221762-seodomination](https://java.net/people/1221762-seodomination)
[http://www.gamespot.com/profile/seodomination/about-me/](http://www.gamespot.com/profile/seodomination/about-me/)
[http://www.calameo.com/accounts/5095871](http://www.calameo.com/accounts/5095871)
[https://www.zotero.org/seodomination](https://www.zotero.org/seodomination)
[https://myspace.com/seodomination](https://myspace.com/seodomination)

With SEO for Craft’s sitemap manager you have complete control over what content you want to have appear on your sitemap as well as managing its change frequency and priority in your site.

Before using the SEO field type, you’ll need to ensure all the settings are correct. You can find the settings under the SEO plugin menu in the sidebar, or via the plugin menu.

[http://findit.arbroathherald.co.uk/company/941973748043776](http://findit.arbroathherald.co.uk/company/941973748043776)
[http://findit.midlothianadvertiser.co.uk/company/941973748043776](http://findit.midlothianadvertiser.co.uk/company/941973748043776)
[http://findit.linlithgowgazette.co.uk/company/941973748043776](http://findit.linlithgowgazette.co.uk/company/941973748043776)
[http://findit.driffieldtoday.co.uk/company/941973748043776](http://findit.driffieldtoday.co.uk/company/941973748043776)
[http://findit.bedfordtoday.co.uk/company/941973748043776](http://findit.bedfordtoday.co.uk/company/941973748043776)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.